from pip._vendor import requests
from random import randint
import random

def basicDownload( name, url):
    if url == None:
        print("notfound")
    else:
        encoded_image = requests.get(url).content
        image = open(name, 'wb')
        image.write(encoded_image)

def downloadFile(url, Custom_name, ID_length):
    if url == None:
        pass
    else:
        randID = ""
        randNum = 0
        randLet = ""
        string = "abcdefghijklmnopqrstuvwxyz"
        pngFileList = []
        if Custom_name != None:
            for i in range(ID_length):
                randDecide = randint(0, 20)
                if randDecide % 2:
                    randNum = randint(0, 9)
                    pngFileList.append(str(randNum))
                else:
                    randLet = random.choice(string)
                    pngFileList.append(randLet)
            for i in pngFileList:
                randID += i
            encoded_image = requests.get(url).content
            image = open(randID, 'wb')
        else:
            encoded_image = requests.get(url).content
            image = open(Custom_name, 'wb')
        image.write(encoded_image)
