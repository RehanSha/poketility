import pygame


def playsound(sound):
    pygame.mixer.init()
    my_sound = pygame.mixer.Sound(sound)
    my_sound.play()
    pygame.time.wait(int(my_sound.get_length() * 1000))
