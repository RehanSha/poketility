from pip._vendor import requests
import Sound_tests as ST
import File_tests as FT
import Image_Downloading as ImgDld
typees = requests.get("https://pokeapi.co/api/v2/type").json()
print("\n")
NewPokedex = None
DiscoveredPokemon = {}
DiscoveredPokemonDescription = {}


def textUseBeginning():
    print('Hello and welcome to the PokeTility App!')
    print(' Learn tons of information about pokemon thorugh this app!')
    print('Remember, any time that you are entering an answer to a question, answer in the lower case,')
textUseBeginning()


class Pokedex():
    def __init__(self):
        pass
    def AskForInfo(self):
        self.askForData = input('What would you like to learn about pokemon?')
        self.CheckInfo()
    def fileWrite(self, file, words, editType):
        outfile = open(file, editType)
        outfile.write(words)
        outfile.write("\n")
        outfile.close()
    def GetTypeByName(self):
        typeRequest = input('What type would you like to learn about?')
        ChosenType = requests.get("https://pokeapi.co/api/v2/type/" + typeRequest).json()
        Damage_Relation_Key = requests.get("https://pokeapi.co/api/v2/type/"  + typeRequest).json()['damage_relations']
        def Print(message, search_term):
            print(message + ':', ChosenType[search_term])
        Print('name', 'name')
        print('weakness(es):')
        for types in Damage_Relation_Key['double_damage_from']:
            print('       ' + types['name'])
        print('strength(s):')
        for types in Damage_Relation_Key['double_damage_to']:
            print('       ' + types['name'])
        if len(Damage_Relation_Key['no_damage_from']) != 0:
            print('immunity:')
            for item in Damage_Relation_Key['no_damage_from']:
                print('       ' + item['name'])
        if len(Damage_Relation_Key['no_damage_to']) != 0:
            print('useless against:')
            for item in Damage_Relation_Key['no_damage_to']:
                print('       ' + item['name'])
        ST.playsound('Sound.wav')
        self.AskForInfo()
    def GetPokemonByName(self):
        PokeRequest = input('Which pokemon do you want to learn about?')
        ChosenPokemon = requests.get("https://pokeapi.co/api/v2/pokemon/" + PokeRequest).json()
        PokemonType = requests.get("https://pokeapi.co/api/v2/pokemon/" + PokeRequest).json()['types']
        PokemonMoves = requests.get("https://pokeapi.co/api/v2/pokemon/" + PokeRequest).json()['moves']
        Weight = int(ChosenPokemon["weight"]) / 10
        print("Number:", ChosenPokemon['id'])
        print("Height:", ChosenPokemon['height'], " Decimeters")
        print("Weight:", Weight, "Kilograms")
        print('Name:', ChosenPokemon['name'])
        print("Types:")
        for i in range(len(PokemonType)):
            print("     " + PokemonType[i]['type']['name'])
        print('Moves:')
        for i in range(len(PokemonMoves)):
            print("     " + PokemonMoves[i]['move']['name'])
        ST.playsound('Sound.wav')
        PokeImgSearch = requests.get("https://pokeapi.co/api/v2/pokemon/" + PokeRequest).json()["sprites"]
        ImageRequest = input("Would you like to download images for this pokemon?")
        if ImageRequest == "yes":
            ImgDld.basicDownload(ChosenPokemon['name'] + 'back.png', ChosenPokemon['sprites']['back_default'])
            ImgDld.basicDownload(ChosenPokemon['name'] + 'backfemale.png', (ChosenPokemon['sprites']['back_female']))
            ImgDld.basicDownload(ChosenPokemon['name'] + 'shinyback.png', ChosenPokemon['sprites']['back_shiny'])
            ImgDld.basicDownload(ChosenPokemon['name'] + 'shinybackfemale.png', ChosenPokemon['sprites']['back_shiny_female'])
            ImgDld.basicDownload(ChosenPokemon['name'] + 'front.png', ChosenPokemon['sprites']['front_default'])
            ImgDld.basicDownload(ChosenPokemon['name'] + 'frontfemale.png', ChosenPokemon['sprites']['front_female'])
            ImgDld.basicDownload(ChosenPokemon['name'] + 'shinyfront.png', ChosenPokemon['sprites']['front_shiny'])
            ImgDld.basicDownload(ChosenPokemon['name'] + 'shinyfrontfemale.png', ChosenPokemon['sprites']['front_shiny_female'])

        self.AskForInfo()
        #print('name:', ChosenPokemon['name'])
    def LogPokemon(self):
        LogRequest = input("What is the name of the pokemon you would like to log?")
        DescriptReq = input("Would you like to give this pokemon a description?")
        LoggedName = requests.get("https://pokeapi.co/api/v2/pokemon/" + LogRequest).json()["name"]

        if DescriptReq == 'yes':
            PokeDescript = input("What would you like to be the description for this pokemon?")
        else:
            PokeDescript = " "
        linkSearch = "https://pokeapi.co/api/v2/pokemon/" + LogRequest
        PokeLog = ""
        PokeLog = PokeDescript
        response = requests.get(linkSearch)
        if response.status_code == 200:
            print("Logging", LogRequest)
            DiscoveredPokemon[LogRequest] = linkSearch
            DiscoveredPokemonDescription[LogRequest] = PokeLog
        else:
            ST.playsound('error.wav')
            print("PokemonNotFoundError: pokemon name does not exist")
        ST.playsound('Sound.wav')
        self.AskForInfo()
    def ReadLog(self):
        print("\n reading your pokemon identification logs...")
        for i in DiscoveredPokemon.keys():
            print(i)
            readReq = input("would you like to retrieve data for this pokemon?")
            if readReq == 'yes':
                pokeReq = requests.get(DiscoveredPokemon[i])
                print("\n")
                print("name:", pokeReq.json()['name'])
                Weight = int(pokeReq.json()["weight"]) / 10
                print("weight:", Weight, "Kilograms")
                print("height:", pokeReq.json()['height'], "decimeters")
                pokeReqType = pokeReq.json()['types']
                print("types:")
                for i in range(len(pokeReqType)):
                    print("    ", pokeReqType[i]['type']['name'])
                print("\n")
                print(DiscoveredPokemonDescription[i])
                print("\n For more information you can do a direct search by using the command 'pokemon' on the \n main pokedex data prompt")
                print("\n")
        print("That's everything you've logged!")
        self.AskForInfo()
    def CementLogs(self):
        self.fileWrite("Log.txt", "", "w")        
        for item in DiscoveredPokemonDescription.keys():    
            self.fileWrite("Log.txt", item, "a")
            self.fileWrite("Log.txt", DiscoveredPokemonDescription[item], "a")
            self.fileWrite("Log.txt", "\n", "a")
    def GetMoveByName(self):
        WordAnylizer = []
        MoveSearchReq = input("What is the name or id of the move you would like to search?")
        for letter in MoveSearchReq:
            WordAnylizer.append(letter)
        for i in range(len(WordAnylizer)):
            if WordAnylizer[i] == " ":
                WordAnylizer[i] = "-"
        MoveSearchReq = ""
        for i in range(len(WordAnylizer)):
            MoveSearchReq += WordAnylizer[i]
        MoveSearchURL = requests.get("https://pokeapi.co/api/v2/move/" + MoveSearchReq).json()
        MoveTypeSearchURL = MoveSearchURL["type"]
        MoveClassSearchURL = MoveSearchURL["damage_class"]
        print("\n")
        print(MoveSearchURL["name"])
        print("Accuracy:", MoveSearchURL["accuracy"])
        print("Type:", MoveTypeSearchURL["name"])
        print("Damage Type:", MoveClassSearchURL["name"])
        print("Power:", MoveSearchURL["power"])
        print("PP:", MoveSearchURL["pp"])
        print("\n")
        print("Move descript:")
        for i in MoveSearchURL["effect_entries"]:
            print(i["effect"])
            print(i["short_effect"])
        print("\n")
        ST.playsound("Sound.wav")
        self.AskForInfo()
    def CheckInfo(self):
        if self.askForData == 'type':
            self.GetTypeByName()
        elif self.askForData == 'pokemon':
            self.GetPokemonByName()
        elif self.askForData == 'log':
            self.LogPokemon()
        elif self.askForData == 'read log':
            self.ReadLog()
        elif self.askForData == 'moves':
            self.GetMoveByName()
        elif self.askForData == 'nothing':
            self.CementLogs()
            del self
        else:
            ST.playsound('error.wav')
            print('NameError: "', self.askForData, '" not defined')
            self.AskForInfo()

def request():
    print('\n')
    Utitility = input('Which Utility would you like to access?')
    if Utitility == 'pokedex':
        global NewPokedex
        NewPokedex = Pokedex()
    else:
        print('\n')
        print('We do not have such a Utility or you have mispelled it. Please reread the \n instructions before submitting your request so that we can provide you with the service you need.')
        

request()
NewPokedex.AskForInfo()
